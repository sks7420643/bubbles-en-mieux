package fr.iut.rodez.bubbles.fx.components;

import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class GlassText extends Text {

    public GlassText(String text) {
        super(text);
        setFill(javafx.scene.paint.Color.WHITE);
        setFont(Font.font("Arial", FontWeight.BLACK, 14));
    }
}
