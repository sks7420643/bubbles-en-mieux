package fr.iut.rodez.bubbles.fx.model;

import fr.iut.rodez.bubbles.domain.Position;
import fr.iut.rodez.bubbles.fx.graphics.Drawable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public record GraphicalRelation(
        GraphicalFamilyMember source,
        GraphicalFamilyMember target
) implements Drawable {

    @Override
    public void drawWith(GraphicsContext context) {
        context.setStroke(Color.WHITE);
        context.setLineWidth(4.0);
        Position sourcePosition = source.currentPosition();
        Position targetPosition = target.currentPosition();
        context.strokeLine(sourcePosition.x(), sourcePosition.y(), targetPosition.x(), targetPosition.y());
    }

    @Override
    public boolean covers(Position position) {
        Position sourcePosition = source.currentPosition();
        Position targetPosition = target.currentPosition();
        double x = position.x() - sourcePosition.x();
        double y = position.y() - sourcePosition.y();
        double a = targetPosition.x() - sourcePosition.x();
        double b = targetPosition.y() - sourcePosition.y();
        double t = (x * a + y * b) / (a * a + b * b);
        double xProj = sourcePosition.x() + t * a;
        double yProj = sourcePosition.y() + t * b;
        return position.distanceTo(new Position(xProj, yProj)) <= 4.0;
    }
}
