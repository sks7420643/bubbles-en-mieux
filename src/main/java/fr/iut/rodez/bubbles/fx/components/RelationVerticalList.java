package fr.iut.rodez.bubbles.fx.components;

import fr.iut.rodez.bubbles.fx.commons.Backgrounds;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamily;
import fr.iut.rodez.bubbles.fx.model.GraphicalRelation;
import javafx.geometry.Pos;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;

public class RelationVerticalList extends ScrollPane {

    private static final int DEFAULT_SPACING = 16;

    public RelationVerticalList(GraphicalFamily family) {
        setBackground(Backgrounds.TRANSPARENT);

        VBox vbox = new VBox(DEFAULT_SPACING);
        vbox.setAlignment(Pos.TOP_RIGHT);
        vbox.setBackground(Backgrounds.TRANSPARENT);
        vbox.setFillWidth(false);

        vbox.getChildren()
            .addAll(family.relations.stream()
                                    .map(relation -> new RelationDescriptionBox(relation.source(), relation.target(), "linked to"))
                                    .toList());

        family.relations.addListener((observable, oldValue, newValue) -> {
            vbox.getChildren()
                .clear();
            for (GraphicalRelation relation : newValue) {
                vbox.getChildren()
                    .add(new RelationDescriptionBox(relation.source(), relation.target(), "linked to"));
            }
        });

        setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        setFitToWidth(true);
        setContent(vbox);
    }
}
