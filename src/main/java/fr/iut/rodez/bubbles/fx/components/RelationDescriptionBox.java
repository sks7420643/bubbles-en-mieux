package fr.iut.rodez.bubbles.fx.components;

import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.fx.commons.Backgrounds;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamilyMember;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Rectangle;

public class RelationDescriptionBox extends HBox {

    private static final int DEFAULT_SPACING = 8;

    public RelationDescriptionBox(GraphicalFamilyMember from, GraphicalFamilyMember to, String relation) {
        super(DEFAULT_SPACING);
        setAlignment(Pos.CENTER_LEFT);
        setBackground(Backgrounds.GLASS);

        Rectangle glassShape = new Rectangle();
        glassShape.widthProperty()
                  .bind(widthProperty());
        glassShape.heightProperty()
                  .bind(heightProperty());
        glassShape.setArcWidth(64);
        glassShape.setArcHeight(64);
        setClip(glassShape);

        getChildren().addAll(new FamilyMemberBox(from), new GlassText(relation), new FamilyMemberBox(to, FamilyMemberBox.Orientation.AVATAR_ON_RIGHT));
    }
}
