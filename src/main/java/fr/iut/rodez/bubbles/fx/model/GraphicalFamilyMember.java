package fr.iut.rodez.bubbles.fx.model;

import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.domain.Identity;
import fr.iut.rodez.bubbles.domain.Position;
import fr.iut.rodez.bubbles.domain.Social;
import fr.iut.rodez.bubbles.fx.graphics.Draggable;
import javafx.scene.canvas.GraphicsContext;

public class GraphicalFamilyMember implements Draggable, Social<GraphicalFamilyMember> {

    private static final double CIRCLE_RADIUS = 32.0;

    private final FamilyMember familyMember;

    public GraphicalFamilyMember(FamilyMember familyMember) {
        this.familyMember = familyMember;
    }

    public Position currentPosition() {
        return familyMember.currentPosition();
    }

    public Identity identity() {
        return familyMember.identity;
    }

    @Override
    public void drawWith(GraphicsContext context) {
        Position pos = currentPosition();
        context.fillOval(pos.x() - CIRCLE_RADIUS, pos.y() - CIRCLE_RADIUS, CIRCLE_RADIUS * 2, CIRCLE_RADIUS * 2);
    }

    @Override
    public boolean covers(Position position) {
        return currentPosition().distanceTo(position) <= CIRCLE_RADIUS;
    }

    @Override
    public void moveTo(Position position) {
        familyMember.moveTo(position);
    }

    @Override
    public void parentOf(GraphicalFamilyMember other) {
        familyMember.parentOf(other.familyMember);
    }
}
