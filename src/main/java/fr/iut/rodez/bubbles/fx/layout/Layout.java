package fr.iut.rodez.bubbles.fx.layout;

import fr.iut.rodez.bubbles.fx.commons.Backgrounds;
import fr.iut.rodez.bubbles.fx.components.BottomAppBar;
import fr.iut.rodez.bubbles.fx.components.RelationVerticalList;
import fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas;
import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamily;
import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;

import static fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas.EventManagementStrategy.NODE_MANIPULATION;

public class Layout extends BorderPane {

    public Layout(Family family) {
        setPadding(new Insets(8));
        setBackground(Backgrounds.TRANSPARENT);

        Pane center = new Pane();

        GraphicalFamily graphicalFamily = new GraphicalFamily(family);

        var graph = new FamilyGraphResizableCanvas(graphicalFamily, NODE_MANIPULATION);

        graph.widthProperty().bind(center.widthProperty());
        graph.heightProperty().bind(center.heightProperty());

        center.getChildren()
              .add(graph);

        BottomAppBar bottomAppBar = new BottomAppBar();
        bottomAppBar.setOnEventManagementStrategyChange(graph::setEventManagementStrategy);

        setRight(createContainerFor(new RelationVerticalList(graphicalFamily)));
        setCenter(createContainerFor(center));
        setBottom(createContainerFor(bottomAppBar));
    }

    public Region createContainerFor(Region region) {
        BorderPane pane = new BorderPane(region);
        pane.setPadding(new Insets(8));
        return pane;
    }
}
