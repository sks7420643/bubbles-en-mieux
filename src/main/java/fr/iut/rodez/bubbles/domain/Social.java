package fr.iut.rodez.bubbles.domain;

public interface Social<T> {

    void parentOf(T other);
}
