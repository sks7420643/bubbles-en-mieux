package fr.iut.rodez.bubbles.domain;

import static fr.iut.rodez.bubbles.utils.Validations.requirePositive;

public record Position(
        double x,
        double y
)  {

    public Position {
        requirePositive(x, () -> "X coordinate must be positive.");
        requirePositive(y, () -> "Y coordinate must be positive.");
    }

    public double distanceTo(Position position) {
        return Math.sqrt(Math.pow(x - position.x, 2) + Math.pow(y - position.y, 2));
    }
}
