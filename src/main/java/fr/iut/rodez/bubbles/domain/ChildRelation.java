package fr.iut.rodez.bubbles.domain;

public final class ChildRelation extends Relation {

    public ChildRelation(FamilyMember other) {
        super(other);
    }
}
